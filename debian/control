Source: python-persist-queue
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Martin <debacle@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
	dh-python,
	python3-all,
  python3-dbutils,
	python3-mock,
	python3-msgpack,
	python3-nose2,
	python3-setuptools,
  python3-cbor2,
Standards-Version: 4.2.1
Rules-Requires-Root: no
Homepage: https://github.com/peter-wangxu/persist-queue
Vcs-Git: https://salsa.debian.org/python-team/packages/python-persist-queue.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-persist-queue

Package: python3-persist-queue
Architecture: all
Depends: ${misc:Depends},
	${python3:Depends},
Description: thread-safe disk based persistent queue
 persist-queue implements a file-based queue and a serial of
 sqlite3-based queues. The goals is to achieve following
 requirements:
 .
  * Disk-based: each queued item should be stored in disk in
    case of any crash.
  * Thread-safe: can be used by multi-threaded producers and
    multi-threaded consumers.
  * Recoverable: Items can be read after process restart.
  * Green-compatible: can be used in greenlet or eventlet
    environment.
